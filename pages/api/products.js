const wooConfig = require('../../WooConfig');
const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;

const WooCommerce = new WooCommerceRestApi({
  url: wooConfig.siteUrl,
  consumerKey: wooConfig.consumerKey,
  consumerSecret: wooConfig.consumerSecret,
  version: 'wc/v3'
});

export default async(req, res) => {
    await WooCommerce.get("products")
    .then((response) => {
        res.send(response.data)
    })
    .catch((error) => {
        res.send({msg: error})
    });
}

