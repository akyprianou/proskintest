import Layout from "../components/Layout";
import fetch from 'isomorphic-unfetch';
import absoluteUrl from 'next-absolute-url';
import Products from "../components/Products";

const Index = (props) => {
  
  const {products} = props;

  if (products.length > 0){

  return(
    <Layout>
      <div className="main-title">
        <h1>Hello next app</h1>
        <div className="row">
        {products.length ? (products.map((product)=>{
          return <Products key={product.id} product={product} />
        })) : ''}
        </div>
      </div>
    </Layout>
  )
}

return 'loading...';


}

  Index.getInitialProps = async({req}) => {
    const { origin } = absoluteUrl(req);
    const data = await fetch(`${origin}/api/products`)
    const productsData = await data.json();

    return {
      products: productsData
    }
  }

export default Index;
