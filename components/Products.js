const Products = (props) => {
  const { product } = props;
  return (
    <div className="col-md-4">
      <h1>{product.name}</h1>
      <p>{product.id}</p>
      <p>{product.images.length > 0 ? <img src={product.images[0].src} height="200" /> : null} </p>
        {/* <p></p> */}
      <div dangerouslySetInnerHTML={{__html: product.price_html }} />
    </div>
  );
};

export default Products;
