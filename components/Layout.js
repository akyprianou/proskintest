import Head from 'next/head';
import Navbar from "./Navbar";

const Layout = (props) => {
  return(<div>
    <Head>
      <title>Pro Skin Sphere</title>
      <link rel="icon" href="/favicon.ico" />
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" />
    </Head>
    <Navbar />
    <div className="container">
      {props.children}
    </div>
  </div>)
};

export default Layout;
