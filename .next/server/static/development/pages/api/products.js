module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./WooConfig.js":
/*!**********************!*\
  !*** ./WooConfig.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/*server credentials*/\nconst serverCredentials = {\n  siteUrl: 'https://amaryllisspa.com/',\n  consumerKey: 'ck_ad33596175d3e0da34a8d6f6654a478e133c4f65',\n  consumerSecret: 'cs_1482e09ec5d815921dbc908f106615328cc3b901'\n};\nconst clientCredentials = {\n  siteUrl: 'http://localhost:8080/amaryllis-shop/',\n  consumerKey: 'ck_cba04d409f4be10e89d9b1de39a60ba86a7fc10d',\n  consumerSecret: 'cs_932045b437670afeec1dc264f5a93ee8cdce8d39'\n};\nconst WooConfig = serverCredentials;\nmodule.exports = WooConfig;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9Xb29Db25maWcuanM/MTNmNCJdLCJuYW1lcyI6WyJzZXJ2ZXJDcmVkZW50aWFscyIsInNpdGVVcmwiLCJjb25zdW1lcktleSIsImNvbnN1bWVyU2VjcmV0IiwiY2xpZW50Q3JlZGVudGlhbHMiLCJXb29Db25maWciLCJtb2R1bGUiLCJleHBvcnRzIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBLE1BQU1BLGlCQUFpQixHQUFHO0FBQ3hCQyxTQUFPLEVBQUUsMkJBRGU7QUFFeEJDLGFBQVcsRUFBRSw2Q0FGVztBQUd4QkMsZ0JBQWMsRUFBRTtBQUhRLENBQTFCO0FBTUEsTUFBTUMsaUJBQWlCLEdBQUc7QUFDeEJILFNBQU8sRUFBRSx1Q0FEZTtBQUV4QkMsYUFBVyxFQUFFLDZDQUZXO0FBR3hCQyxnQkFBYyxFQUFFO0FBSFEsQ0FBMUI7QUFNQSxNQUFNRSxTQUFTLEdBQUdMLGlCQUFsQjtBQUVBTSxNQUFNLENBQUNDLE9BQVAsR0FBaUJGLFNBQWpCIiwiZmlsZSI6Ii4vV29vQ29uZmlnLmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypzZXJ2ZXIgY3JlZGVudGlhbHMqL1xuY29uc3Qgc2VydmVyQ3JlZGVudGlhbHMgPSB7XG4gIHNpdGVVcmw6ICdodHRwczovL2FtYXJ5bGxpc3NwYS5jb20vJyxcbiAgY29uc3VtZXJLZXk6ICdja19hZDMzNTk2MTc1ZDNlMGRhMzRhOGQ2ZjY2NTRhNDc4ZTEzM2M0ZjY1JyxcbiAgY29uc3VtZXJTZWNyZXQ6ICdjc18xNDgyZTA5ZWM1ZDgxNTkyMWRiYzkwOGYxMDY2MTUzMjhjYzNiOTAxJyBcbn1cblxuY29uc3QgY2xpZW50Q3JlZGVudGlhbHMgPSB7XG4gIHNpdGVVcmw6ICdodHRwOi8vbG9jYWxob3N0OjgwODAvYW1hcnlsbGlzLXNob3AvJyxcbiAgY29uc3VtZXJLZXk6ICdja19jYmEwNGQ0MDlmNGJlMTBlODlkOWIxZGUzOWE2MGJhODZhN2ZjMTBkJyxcbiAgY29uc3VtZXJTZWNyZXQ6ICdjc185MzIwNDViNDM3NjcwYWZlZWMxZGMyNjRmNWE5M2VlOGNkY2U4ZDM5JyBcbn1cblxuY29uc3QgV29vQ29uZmlnID0gc2VydmVyQ3JlZGVudGlhbHM7XG5cbm1vZHVsZS5leHBvcnRzID0gV29vQ29uZmlnOyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./WooConfig.js\n");

/***/ }),

/***/ "./pages/api/products.js":
/*!*******************************!*\
  !*** ./pages/api/products.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst wooConfig = __webpack_require__(/*! ../../WooConfig */ \"./WooConfig.js\");\n\nconst WooCommerceRestApi = __webpack_require__(/*! @woocommerce/woocommerce-rest-api */ \"@woocommerce/woocommerce-rest-api\").default;\n\nconst WooCommerce = new WooCommerceRestApi({\n  url: wooConfig.siteUrl,\n  consumerKey: wooConfig.consumerKey,\n  consumerSecret: wooConfig.consumerSecret,\n  version: 'wc/v3'\n});\n/* harmony default export */ __webpack_exports__[\"default\"] = (async (req, res) => {\n  await WooCommerce.get(\"products\").then(response => {\n    res.send(response.data);\n  }).catch(error => {\n    res.send({\n      msg: error\n    });\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy9hcGkvcHJvZHVjdHMuanM/ZjYzZiJdLCJuYW1lcyI6WyJ3b29Db25maWciLCJyZXF1aXJlIiwiV29vQ29tbWVyY2VSZXN0QXBpIiwiZGVmYXVsdCIsIldvb0NvbW1lcmNlIiwidXJsIiwic2l0ZVVybCIsImNvbnN1bWVyS2V5IiwiY29uc3VtZXJTZWNyZXQiLCJ2ZXJzaW9uIiwicmVxIiwicmVzIiwiZ2V0IiwidGhlbiIsInJlc3BvbnNlIiwic2VuZCIsImRhdGEiLCJjYXRjaCIsImVycm9yIiwibXNnIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQU1BLFNBQVMsR0FBR0MsbUJBQU8sQ0FBQyx1Q0FBRCxDQUF6Qjs7QUFDQSxNQUFNQyxrQkFBa0IsR0FBR0QsbUJBQU8sQ0FBQyw0RUFBRCxDQUFQLENBQTZDRSxPQUF4RTs7QUFFQSxNQUFNQyxXQUFXLEdBQUcsSUFBSUYsa0JBQUosQ0FBdUI7QUFDekNHLEtBQUcsRUFBRUwsU0FBUyxDQUFDTSxPQUQwQjtBQUV6Q0MsYUFBVyxFQUFFUCxTQUFTLENBQUNPLFdBRmtCO0FBR3pDQyxnQkFBYyxFQUFFUixTQUFTLENBQUNRLGNBSGU7QUFJekNDLFNBQU8sRUFBRTtBQUpnQyxDQUF2QixDQUFwQjtBQU9lLHNFQUFNQyxHQUFOLEVBQVdDLEdBQVgsS0FBbUI7QUFDOUIsUUFBTVAsV0FBVyxDQUFDUSxHQUFaLENBQWdCLFVBQWhCLEVBQ0xDLElBREssQ0FDQ0MsUUFBRCxJQUFjO0FBQ2hCSCxPQUFHLENBQUNJLElBQUosQ0FBU0QsUUFBUSxDQUFDRSxJQUFsQjtBQUNILEdBSEssRUFJTEMsS0FKSyxDQUlFQyxLQUFELElBQVc7QUFDZFAsT0FBRyxDQUFDSSxJQUFKLENBQVM7QUFBQ0ksU0FBRyxFQUFFRDtBQUFOLEtBQVQ7QUFDSCxHQU5LLENBQU47QUFPSCxDQVJEIiwiZmlsZSI6Ii4vcGFnZXMvYXBpL3Byb2R1Y3RzLmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3Qgd29vQ29uZmlnID0gcmVxdWlyZSgnLi4vLi4vV29vQ29uZmlnJyk7XG5jb25zdCBXb29Db21tZXJjZVJlc3RBcGkgPSByZXF1aXJlKFwiQHdvb2NvbW1lcmNlL3dvb2NvbW1lcmNlLXJlc3QtYXBpXCIpLmRlZmF1bHQ7XG5cbmNvbnN0IFdvb0NvbW1lcmNlID0gbmV3IFdvb0NvbW1lcmNlUmVzdEFwaSh7XG4gIHVybDogd29vQ29uZmlnLnNpdGVVcmwsXG4gIGNvbnN1bWVyS2V5OiB3b29Db25maWcuY29uc3VtZXJLZXksXG4gIGNvbnN1bWVyU2VjcmV0OiB3b29Db25maWcuY29uc3VtZXJTZWNyZXQsXG4gIHZlcnNpb246ICd3Yy92Mydcbn0pO1xuXG5leHBvcnQgZGVmYXVsdCBhc3luYyhyZXEsIHJlcykgPT4ge1xuICAgIGF3YWl0IFdvb0NvbW1lcmNlLmdldChcInByb2R1Y3RzXCIpXG4gICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgIHJlcy5zZW5kKHJlc3BvbnNlLmRhdGEpXG4gICAgfSlcbiAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgIHJlcy5zZW5kKHttc2c6IGVycm9yfSlcbiAgICB9KTtcbn1cblxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/api/products.js\n");

/***/ }),

/***/ 4:
/*!*************************************!*\
  !*** multi ./pages/api/products.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/alexkey89/Desktop/NEXTJSVercelProskin/proskintest/pages/api/products.js */"./pages/api/products.js");


/***/ }),

/***/ "@woocommerce/woocommerce-rest-api":
/*!****************************************************!*\
  !*** external "@woocommerce/woocommerce-rest-api" ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"@woocommerce/woocommerce-rest-api\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAd29vY29tbWVyY2Uvd29vY29tbWVyY2UtcmVzdC1hcGlcIj82MzIzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6IkB3b29jb21tZXJjZS93b29jb21tZXJjZS1yZXN0LWFwaS5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkB3b29jb21tZXJjZS93b29jb21tZXJjZS1yZXN0LWFwaVwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///@woocommerce/woocommerce-rest-api\n");

/***/ })

/******/ });