const next = require('next');
const express = require('express');
const wooConfig = require('./WooConfig');

const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;

const WooCommerce = new WooCommerceRestApi({
  url: wooConfig.siteUrl,
  consumerKey: wooConfig.consumerKey,
  consumerSecret: wooConfig.consumerSecret,
  version: 'wc/v3'
});

const port = 5002;
const dev = process.env.NODE_EVN !== 'production';
const app = next({dev});
const handle = app.getRequestHandler();

app.prepare().then(()=>{
  const server = express();

  server.get('/getProducts', (req, res)=>{
    WooCommerce.get("products")
    .then((response) => {
      res.send(response.data)
    })
    .catch((error) => {
      res.send({msg: error})
    });
  })


  server.get('*', (req, res)=>{
    return handle(req, res);
  })

  server.listen(port, err=>{
    if (err) throw err;
    console.log('listening on http://localhost:' + port)
  })


}).catch(ex=>{
  console.error(ex.stack);
  process.exit(1);
})
